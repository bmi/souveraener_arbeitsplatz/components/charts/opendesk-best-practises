<!--
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
-->
# Required and optional templates in Helm Charts

This document provides an overview of the templates all helm charts use in openDesk should contain and explains
these templates and their values.

[TOC]

## Repository structure

We suggest using the [gitlab-config](https://gitlab.souvap-univention.de/souvap/tooling/gitlab-config) Gitlab-CI
configuration templates. By using this, a specific repository structure is required:

```text
<repository root>/
├── charts/
│   └── <chart>/
│       ├── files/
│       ├── templates/
│       ├── Chart.yaml
│       ├── LICENSE
│       ├── README.md.gotmpl.tpl
│       └── values.yaml
├── LICENSES/
└── .gitlab-ci.yml
```

Placing the actual chart in the `charts/` folder is common sense across multiple large open-source repositories.

The chart's folder has to have the same name as the value of the `name` attribute from the chart's `Chart.yaml` file.

## Template sections

The following list contains commonly used Kubernetes keywords and their optimized helm templates:

### Security

The goal of openDesk is to have a secure and sovereign suite. This forces suppliers of openDesk components to comply with
common security standards, like "IT-Grundschutz". To comply with Kubernetes security standards, you should include as
many of the following sections as possible.

#### containerSecurityContext

The containerSecurityContext is the most important security-related section because it has the highest precedence and
restricts the container to its minimal privileges. Therefore, this section has to be reviewed carefully and has to be
present in any openDesk related charts.

This example shows the complete containerSecurityContext with the ideal security restraints. Depending on your
application, you may not be able to use the same config today, in that case you should at least start thinking
about how to further develop your application's image in order to apply these settings in the future.

```yaml
# Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
containerSecurityContext:
  # -- Enable container privileged escalation.
  allowPrivilegeEscalation: false

  # -- Security capabilities for container.
  capabilities:
    drop:
      - "ALL"

  # -- Enable security context.
  enabled: true

  # -- Restrict privileges of container.
  privileged: false

  # -- Process user id.
  runAsUser: 1000

  # -- Process group id.
  runAsGroup: 1000

  # Set Seccomp profile.
  seccompProfile:
    # -- Disallow custom Seccomp profile by setting it to RuntimeDefault.
    type: "RuntimeDefault"

  # -- Mounts the container's root filesystem as read-only.
  readOnlyRootFilesystem: true

  # -- Run container as a user.
  runAsNonRoot: true
```
##### allowPrivilegeEscalation

Privilege escalation (such as via set-user-ID or set-group-ID file mode) should not be allowed (Linux only) at any time.

```yaml
containerSecurityContext:
  allowPrivilegeEscalation: false
```

##### capabilities

Containers must drop ALL capabilities, and are only permitted to add back the `NET_BIND_SERVICE` capability (Linux
only).

Optimal:

```yaml
containerSecurityContext:
  capabilities:
    drop:
      - "ALL"
```

Allowed:

```yaml
containerSecurityContext:
  capabilities:
    drop:
      - "ALL"
    add:
      - "NET_BIND_SERVICE"
```

##### privileged

Privileged Pods disable most security mechanisms and must be disallowed.

```yaml
containerSecurityContext:
  privileged: false
```

##### runAsUser

Containers should set a user id >= 1000 and never use 0 (root) as user.

```yaml
containerSecurityContext:
  runAsUser: 1000
```

##### runAsGroup

Containers should set a group id >= 1000 and never use 0 (root) as user.

```yaml
containerSecurityContext:
  runAsGroup: 1000
```

##### seccompProfile

Seccomp profile must be explicitly set to one of the allowed values. An unconfined profile and the complete absence of
the profile are prohibited.

```yaml
containerSecurityContext:
  seccompProfile:
    type: "RuntimeDefault"
```

or

```yaml
containerSecurityContext:
  seccompProfile:
    type: "Localhost"
```

##### readOnlyRootFilesystem

Containers should have an immutable file systems, so that attackers could not modify application code or download
malicious code.

```yaml
containerSecurityContext:
  readOnlyRootFilesystem: true
```

##### runAsNonRoot

Containers must be required to run as non-root users.

```yaml
containerSecurityContext:
  runAsNonRoot: true
```

#### podSecurityContext

The podSecurityContext allows setting the same values as [containerSecurityContext](#containerSecurityContext) and they
are applied to each container.
PodSecurityContext has lower precedence than containerSecurityContext and thus setting here can be overloaded in
containerSecurityContext and gain higher privileges.

This example shows the podSecurityContext filled with its ideal values:

```yaml
# Pod Security Context.
# Ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
podSecurityContext:
  # -- Enable security context.
  enabled: true

  # -- If specified, all processes of the container are also part of the supplementary group
  fsGroup: 1000
```

##### fsGroup

When fsGroup field is specified, all processes of the container are also part of the supplementary group ID.

By default, Kubernetes recursively changes ownership and permissions for the contents of each volume at mount time
to match the fsGroup specified value from the Pod's securityContext.

```yaml
podSecurityContext:
  fsGroup: 1000
```

### Global

Global values are values that can be accessed from any chart or subchart by exactly the same name.
Globals require explicit declaration. 
You cannot use an existing non-global as if it were a global value.

Some keys are standard within the Helm charts specifically developed for openDesk, so ideally, all Helm charts that are
used by openDesk - even the ones not specifically developed for that use case - provide these global keys.

```yaml
# The global properties are used to configure multiple charts at once.
global:
  # -- Container registry address.
  imageRegistry: "docker.io"

  # -- Credentials to fetch images from private registry
  # Ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  #
  # imagePullSecrets:
  #   - "docker-registry"
  #
  imagePullSecrets: []
```

#### imageRegistry

Setting the imageRegistry on a global level allows the users of the chart to replace the original registry by
a proxy or a local registry.

It is mandatory to provide a registry. When using images from DockerHub, you can set `docker.io` as imageRegistry.

```yaml
global:
  imageRegistry: "docker.io"
```

#### imagePullSecrets

When using a private registry that requires authentication for pulling images, you need to [define secrets within
your Kubernetes cluster](https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/#create-a-secret-by-providing-credentials-on-the-command-line)
and then reference the secret(s) by name.

The names of the Secrets can be added as a list. Multiple values are possible.

```yaml
global:
  imagePullSecrets:
    - "my-private-registry"
    - "my-second-registry"
```

### Image

When you need to deploy a Pod, it is common practice to support all image related configurations.

```yaml
# Container image section.
image:
  # -- Container registry address. This setting has higher precedence than global.registry.
  registry: ""

  # -- Container repository string.
  repository: "alpine"

  # -- Define an ImagePullPolicy.
  imagePullPolicy: "IfNotPresent"

  # -- Define image tag.
  tag: "3.18.4@sha256:48d9183eb12a05c99bcc0bf44a003607b8e941e1d4f41f9ad12bdcc4b5672f86"
```

#### registry

It should be possible for the chart's consumer to overload the `global.imageRegistry` setting.

```yaml
image:
  registry: ""
```

#### repository

The repository key contains the name / repository of an image.

```yaml
image:
  repository: "alpine"
```

#### imagePullPolicy

The imagePullPolicy defines when the Kubernetes worker node pulls an image. We recommend setting it to `IfNotPresent`
when you specify a digest, otherwise use `Always`.

- `IfNotPresent`: The image is pulled only if it is not already present locally.
- `Always`: Every time the kubelet launches a container, the kubelet queries the container image registry to resolve the
name to an image digest. If the kubelet has a container image with that exact digest cached locally, the kubelet uses
its cached image; otherwise, the kubelet pulls the image and uses that image to launch the container.
- `Never`: The kubelet does not try fetching the image. If the image is somehow already present locally, the kubelet
attempts to start the container; otherwise, startup fails.

```yaml
image:
  imagePullPolicy: "IfNotPresent"
```

#### tag

The tag defines the version of a container image. When the tag is empty, Kubernetes uses the tag `latest`.

To comply with strict security standards, you have to always use a digest. For better human readability, we enforce to
specify the tag as well as in the following example:

```yaml
image:
  tag: "3.18.4@sha256:48d9183eb12a05c99bcc0bf44a003607b8e941e1d4f41f9ad12bdcc4b5672f86"
```

### Probes

Without probes, Kubernetes does not now the status of your application's deployment.
Therefore, it is mandatory to provide at least `livenessProbe` and `readinessProbe`. 
Depending on the time your application takes to be up and running you should also consider `startupProbe`.

While the consumer of the Helm chart should not interfere with how to probe itself is executed (e.g. `httpGet.*` or
`exec.*`) the values mentioned below should be customizable.

```yaml
#  Configure extra options for containers probes.
livenessProbe:
  # -- Enables kubernetes LivenessProbe.
  enabled: true
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Delay after container start until LivenessProbe is executed.
  initialDelaySeconds: 15
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

#  Configure extra options for containers probes.
readinessProbe:
  # -- Enables kubernetes ReadinessProbe.
  enabled: true
  # -- Delay after container start until ReadinessProbe is executed.
  initialDelaySeconds: 15
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5

#  Configure extra options for containers probes.
startupProbe:
  # -- Enables kubernetes ReadinessProbe.
  enabled: true
  # -- Delay after container start until ReadinessProbe is executed.
  initialDelaySeconds: 15
  # -- Number of failed executions until container is terminated.
  failureThreshold: 10
  # -- Time between probe executions.
  periodSeconds: 20
  # -- Number of successful executions after failed ones until container is marked healthy.
  successThreshold: 1
  # -- Timeout for command return.
  timeoutSeconds: 5
```

#### livenessProbe

The kubelet uses liveness probes to know when to restart a container. For example, liveness probes could catch a
deadlock, where an application is running, but unable to make progress. Restarting a container in such a state can help
to make the application more available despite bugs.

```yaml
livenessProbe:
  enabled: true
  failureThreshold: 10
  initialDelaySeconds: 15
  periodSeconds: 20
  successThreshold: 1
  timeoutSeconds: 5
```

#### readinessProbe

The kubelet uses readiness probes to know when a container is ready to start accepting traffic. A Pod is considered
ready when all of its containers are ready. One use of this signal is to control which Pods are used as backends for
Services. When a Pod is not ready, it is removed from Service load balancers.

```yaml
readinessProbe:
  enabled: true
  failureThreshold: 10
  initialDelaySeconds: 15
  periodSeconds: 20
  successThreshold: 1
  timeoutSeconds: 5
```

#### startupProbe

The kubelet uses startup probes to know when a container application has started. If such a probe is configured,
liveness and readiness probes do not start until it succeeds, making sure those probes don't interfere with the
application startup. This can be used to adopt liveness checks on slow starting containers, avoiding them getting killed
by the kubelet before they are up and running.

```yaml
startupProbe:
  enabled: true
  failureThreshold: 10
  initialDelaySeconds: 15
  periodSeconds: 20
  successThreshold: 1
  timeoutSeconds: 5
```

### Resources

As application workloads share cluster resources, it is important to limit resources requested and consumed by each Pod.
For openDesk it is required to specify default requests and limits per Pod, especially for memory and CPU.

```yaml
# Configure resource requests and limits.
# Ref: https://kubernetes.io/docs/user-guide/compute-resources/
resources:
  limits:
    # -- The max number of CPUs to consume.
    cpu: 1
    # -- The max number of RAM to consume.
    memory: "1Gi"
  requests:
    # -- The number of CPUs which has to be available on the scheduled node.
    cpu: "100m"
    # -- The number of RAM which has to be available on the scheduled node.
    memory: "512Mi"
```
